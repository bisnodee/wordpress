# Bisnode Estonia Wordpress themes and plugins #

A project of integration Bisnode Estonia Credit Reports into most frequently used CMS in the world, Wordpress.

### What is this repository for? ###

* Credit Repoprts plugin
* Authorizator for Estonian customers
* Authorizator for Finnish customers
* Credit reports theme (firmainfo.ee)
* Bisnode Estonia Theme (bisnode.ee) - will be added later

### How do I get set up? ###

* Install wordpress, theme and plugins
* Add your reseller's access token from [Bisnode Estonia Intranet](https://in.bisnode.ee/user/profile) to the Credit Report Plugin settings
* Add users to the system and allow them to get credit reports.

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Dmitri Zinovjev <dmitri.zinovjev@bisnode.com> main contributor
* Sergei Miami <sergei.radchenko@bisnode.com> reviewer