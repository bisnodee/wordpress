<div>
    <h2>Bisnode credit report settings</h2>

    <form method="post" action="options.php">
        <?php wp_nonce_field('update-options'); ?>
        <p>Most of requests to Bisnode Estonia SOAP server require access token, also known as api_key. Please,
            enter your token.</p>
        <p>
            Put shortcode with search form [credit-report] on your Credit report search page.
        </p>
        <table width="510">
            <tr valign="top">
                <th width="92" scope="row">Add token</th>
                <td width="406">
                    <input name="credit_rating_token" type="text" id="credit_rating_token"
                           value="<?php echo get_option('credit_rating_token'); ?>"/>
                </td>
            </tr>
        </table>

        <input type="hidden" name="action" value="update"/>
        <input type="hidden" name="page_options" value="credit_rating_token"/>

        <p>
            <input class="button-primary" type="submit" value="<?php _e('Save Changes') ?>"/>
        </p>

    </form>
</div>