<?php

class Bisnode_Creditreport_Admin
{

    private static $initiated = false;

    public static function init()
    {
        if (!self::$initiated) {
            self::init_hooks();
        }
    }

    private static function init_hooks()
    {
        self::$initiated = true;
        add_action('admin_menu', array('Bisnode_Creditreport_Admin', 'load_menu'));
    }

    public static function load_menu()
    {
        add_options_page('Credit report', 'Credit report', 'administrator',
            'credit-report-settings', array('Bisnode_Creditreport_Admin', 'display_page'));
    }

    public static function display_page()
    {
        Bisnode_Creditreport::view('config');
    }

}
