<?php
/*
Plugin Name: Bisnode credit report
Plugin URI: http://www.bisnode.ee
Description: Bisnode credit report provides information about more then 500 000 companies using soap requests. Feel free to search and get whole information you need. To get started: 1) Click the "Activate" link to the left of this description, 2) Go to your Credit report settings page, and put your token.
Version: 1.0
Author: Bisnode Estonia AS
License: Bisnode Estonia AS
Text Domain: bisnode_creditreport
*/

define('BISNODE_CREDITREPORT__PLUGIN_DIR', plugin_dir_path(__FILE__));
require_once(BISNODE_CREDITREPORT__PLUGIN_DIR . 'class.bisnode_creditreport.php');
require_once(BISNODE_CREDITREPORT__PLUGIN_DIR . 'Bisnode_Soap_Client.php');
require_once(BISNODE_CREDITREPORT__PLUGIN_DIR . 'custom-post-type.php');

register_activation_hook(__FILE__, array('Bisnode_Creditreport', 'plugin_activation'));
register_deactivation_hook(__FILE__, array('Bisnode_Creditreport', 'plugin_deactivation'));


function pluginname_init() {  // name function with your plugin slugname
    $plugin_dir = basename(dirname(__FILE__));
    load_plugin_textdomain( 'bisnode_creditreport', false, $plugin_dir ); // first parameter is your text domain

    // then your code

}
add_action('plugins_loaded', 'pluginname_init'); // second parameter is your init function


add_action('init', array('Bisnode_Creditreport', 'init'));

if (is_admin()) {
    require_once(BISNODE_CREDITREPORT__PLUGIN_DIR . 'class.bisnode_creditreport-admin.php');
    add_action('init', array('Bisnode_Creditreport_Admin', 'init'));
}
