<?php

Class Bisnode_Creditreport
{

    private static $initiated = false;

    /**
     * @var $soap Bisnode_Soap_Client
     */
    private static $soap,
                   $url;

    public static function init()
    {
        if (!self::$initiated) {
            self::init_hooks();
        }
        self::$url = get_site_url() . '/credit-report/';
    }

    public static function init_soap()
    {
        if (!self::$soap) self::$soap = Bisnode_Soap_Client::getInstance()->setToken(get_option('credit_rating_token'));
    }



    private static function init_hooks()
    {
        self::$initiated = true;
        self::create_post_type();

        //remove wordpress authentication
        remove_filter('authenticate', 'wp_authenticate_username_password', 20);

        add_action( 'template_redirect', array('Bisnode_Creditreport', 'redirect_non_logged_users_to_specific_page'));
        add_action( 'manage_report_posts_custom_column', array('Bisnode_Creditreport', 'my_manage_report_columns'), 10, 2);
        add_action( 'show_user_profile', array('Bisnode_Creditreport', 'extra_user_profile_fields' ));
        add_action( 'edit_user_profile', array('Bisnode_Creditreport', 'extra_user_profile_fields' ));
        add_action( 'personal_options_update', array('Bisnode_Creditreport', 'save_extra_user_profile_fields' ));
        add_action( 'edit_user_profile_update', array('Bisnode_Creditreport', 'save_extra_user_profile_fields' ));

        add_shortcode('credit_search', array('Bisnode_Creditreport', 'credit_search'));
        add_shortcode('report_list', array('Bisnode_Creditreport', 'report_list'));

        add_filter('authenticate', array('Bisnode_Creditreport', 'auth_by_email'), 20, 3);
        add_filter('manage_edit-report_columns', array('Bisnode_Creditreport', 'add_new_report_columns'));
        add_filter('show_admin_bar', '__return_false');
        add_filter('query_vars', array('Bisnode_Creditreport', 'report_query_vars'));
        add_filter('parse_query', array('Bisnode_Creditreport', 'query_parser'));
        add_filter('the_posts', array('Bisnode_Creditreport', 'bisnode_page_filter'));
        add_filter('the_content', array('Bisnode_Creditreport', 'filter_me'));

        if(is_admin()){
            add_filter('manage_users_columns' , array('Bisnode_Creditreport', 'add_extra_user_column'));
            add_filter('manage_users_custom_column', array('Bisnode_Creditreport', 'report_columns_content_users'), 10, 3);
            add_filter( 'posts_join', array('Bisnode_Creditreport', 'search_meta_data_join' ) );
            add_filter( 'posts_where', array('Bisnode_Creditreport', 'search_meta_data_where' ) );
        }


    }

    public static function auth_by_email ($user, $email, $password){

        //Check for empty fields
        if(empty($email) || empty ($password)){
            //create new error object and add errors to it.
            $error = new WP_Error();

            if(empty($email)){ //No email
                $error->add('empty_username', __('<strong>ERROR</strong>: Email field is empty.', 'bisnode_creditreport'));
            }
            else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //Invalid Email
                $error->add('invalid_username', __('<strong>ERROR</strong>: Email is invalid.', 'bisnode_creditreport'));
            }

            if(empty($password)){ //No password
                $error->add('empty_password', __('<strong>ERROR</strong>: Password field is empty.', 'bisnode_creditreport'));
            }

            return $error;
        }

        //Check if user exists in WordPress database
        $user = get_user_by('email', $email);
        if(!$user) $user = get_user_by('login', $email);

        //bad email
        if(!$user){
            $error = new WP_Error();
            $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.', 'bisnode_creditreport'));
            return $error;
        }
        else{ //check password
            if(!wp_check_password($password, $user->user_pass, $user->ID)){ //bad password
                $error = new WP_Error();
                $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.', 'bisnode_creditreport'));
                return $error;
            }else{
                return $user; //passed
            }
        }
    }

    /**
     * Adds a join to the WordPress meta table for license key searches in the WordPress Administration
     *
     * @param string $join SQL JOIN statement
     * @return string SQL JOIN statement
     */
    public static function search_meta_data_join($join) {
        global $wpdb;

        // Only join the post meta table if we are performing a search
        $res = get_query_var( 's' );
        if ( empty ( $res ) ) {
            return $join;
        }

        // Only join the post meta table if we are on the Reports Custom Post Type
        if ( 'report' != get_query_var( 'post_type' ) ) {
            return $join;
        }

        // Join the post meta table
        $join .= " LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id ";

        return $join;
    }

    /**
     * Adds a where clause to the WordPress meta table for license key searches in the WordPress Administration
     *
     * @param string $where SQL WHERE clause(s)
     * @return string SQL WHERE clauses
     */
    public static function search_meta_data_where($where) {
        global $wpdb;

        // Only join the post meta table if we are performing a search
        $res = get_query_var( 's' );

        if ( empty ( $res ) ) {
            return $where;
        }

        // Only join the post meta table if we are on the Contacts Custom Post Type
        if ( 'report' != get_query_var( 'post_type' ) ) {
            return $where;
        }

        // Get the start of the query, which is ' AND ((', and the rest of the query
        $startOfQuery = substr( $where, 0, 7 );
        $restOfQuery = substr( $where ,7 );

        // Inject our WHERE clause in between the start of the query and the rest of the query
        $where = $startOfQuery .
            "(" . $wpdb->postmeta . ".meta_value LIKE '%" . get_query_var( 's' ) . "%') OR " . $restOfQuery .
            "GROUP BY " . $wpdb->posts . ".id";

        // Return revised WHERE clause
        return $where;
    }

    public static function add_extra_user_column($columns) {
        $columns['permission']  = 'Report Permission';

        return $columns;
    }

    public static function report_columns_content_users($value, $column_name, $user_id) {
        if ( 'permission' == $column_name ){
            $permission = get_user_meta($user_id,'permission',true) ? get_user_meta($user_id,'permission',true) : '0';
        }
        return $permission;
    }

    public static function extra_user_profile_fields( $user ) {
        if(!current_user_can( 'manage_options' )) return;
        ?>
        <h3><?php _e("Bisnode credit report", "blank"); ?></h3>

        <table class="form-table">
            <tr>
                <th><label for="permission"><?php _e("Report permission"); ?></label></th>
                <td>
                    <select name="permission">
                        <option  value="0" <?php if(esc_attr( !get_the_author_meta( 'permission', $user->ID ))) echo 'selected'; ?>>Forbid</option>
                        <option  value="1"  <?php if(esc_attr( get_the_author_meta( 'permission', $user->ID )) == 1) echo 'selected'; ?>>Allow</option>
                    </select><br />
                    <span class="description"><?php _e(" "); ?></span>
                </td>
            </tr>
        </table>
    <?php }

    public static function save_extra_user_profile_fields( $user_id )
    {
        if (!current_user_can('edit_user', $user_id)) {
            return false;
        }
        update_user_meta($user_id, 'permission', $_POST['permission']);
    }



    public static function my_manage_report_columns( $column, $post_id )
    {
        global $post;

        $duration = get_post_meta($post_id, $column, true);

        /* If no duration is found, output a default message. */
        if (empty($duration))
            echo __('Unknown', 'bisnode_creditreport');

        /* If there is a duration, append 'minutes' to the text string. */
        else
            echo $duration;

    }

    public static function add_new_report_columns($report_columns) {
        $new_columns['cb'] = '<input type="checkbox" />';


        $new_columns['title'] = _x('Business name', 'column name');
        $new_columns['reg_code'] = __('Registration code', 'bisnode_creditreport');
        $new_columns['type'] = __('Report type', 'bisnode_creditreport');
        $new_columns['language'] = __('Language', 'bisnode_creditreport');
        $new_columns['customer'] = __('Customer', 'bisnode_creditreport');



        $new_columns['date'] = _x('Date', 'column name');

        return $new_columns;
    }

    public static function create_post_type() {

        $report = new Custom_Post_Type('report');

        $report->add_taxonomy('report');
        $report->add_meta_box(
            'download',
            array(
                'Pdf link' => 'text',
                'Html link' => 'text',
            )
        );

        $report->register_post_type();
    }



    public static function redirect_non_logged_users_to_specific_page()
    {
        if (!is_user_logged_in() && !is_page('login')) {

            auth_redirect();
            exit;
        }
    }

    public static function filter_me($content){
        return get_the_content();
    }


    /**
     * Include external view file by name
     * @param $name
     */
    public static function view($name)
    {
        $file = BISNODE_CREDITREPORT__PLUGIN_DIR . 'views/' . $name . '.php';
        include($file);
    }

    /**
     * Add database table and rewrite rules
     */
    public static function plugin_activation()
    {
        self::add_new_rule();
    }

    /**
     * Add rewrite rules
     */
    public function add_new_rule()
    {
        add_rewrite_rule(
            'credit-report/?([^/]*)/?([^/]*)/?',
            'index.php?plugin_page=credit-report&act=$matches[1]&reg=$matches[2]',
            'top');

        add_rewrite_rule(
            'report/?([^/]*)',
            'index.php?report=$matches[1]',
            'top');
        global $wp_rewrite;

        $bid_token = '%act%';
        add_rewrite_tag($bid_token, '(.+)', 'act=');

        $bid_token = '%reg%';
        add_rewrite_tag($bid_token, '(.+)', 'reg=');

        $wp_rewrite->flush_rules(true);
    }

    public static function plugin_deactivation()
    {
    }

    /**
     * Add new query var for credit reports
     * @param $qvars
     * @return array
     */
    public static function report_query_vars($qvars)
    {
        $qvars[] = 'plugin_page';
        $qvars[] = 'act';
        $qvars[] = 'reg';
        return $qvars;
    }

    /**
     * Parse query for virtual page
     * @param $q
     */
    public static function query_parser($q)
    {
        if ((isset($q->query_vars['attachment']) && $q->query_vars['attachment'] == 'credit-report') || 
			(isset($q->query_vars['name']) && $q->query_vars['name'] == 'credit-report') ||
			(isset($q->query_vars['plugin_page']) && $q->query_vars['plugin_page'] == 'credit-report')) 
		// if (in_array($q->get('name'), array('credit-report','creditreports')))
		{
            $q->set('virtual_page_is_called', TRUE);
			// var_dump($q);
        } 
        else  
        {
			$q->set('virtual_page_is_called', FALSE);
		}

    }

    /**
     * Set contents for virtual page
     * @param $posts
     * @return $posts
     */
    public static function bisnode_page_filter($posts)
    {
        global $wp_query;
        

        if (isset($_POST['act']) && $_POST['act'] == 'resend')
        {
            $message = 'FINANCIAL INFORMATION IS NOT AVAILABLE ON-LINE DIRECTLY for user: '.$_POST['resend_email'].' and company: '.$_POST['resend_reg'];
            wp_mail('dmitri@bisnode.ee', 'wordpress:EE financial information buy request', $message);
            self::redirect_page();
        }

        if (!$posts) $posts = array(new stdClass());
        
        if (!$wp_query->get('virtual_page_is_called')) return $posts;

        if (!is_user_logged_in()) auth_redirect();

        $posts[0]->post_title = 'Credit report';
        if (get_query_var('act') == 'download') self::download_report(sanitize_text_field(get_query_var('reg')));
        if (isset($_POST['reg'])) {
            $reg = sanitize_text_field($_POST['reg']);
            $type = 'full'; // only full reports for now

            $obj = new stdClass();
            $obj->country = 'EST';
            $obj->language = get_locale() == 'ee_EE' ? 'et' : 'en';
            $obj->reg_code = $reg;
            $obj->customer = wp_get_current_user()->user_email;
            $obj->type = $type;

            self::init_soap();
            try{$result = self::$soap->getCreditReport($obj)->asObject();}
            catch(Exception $e)
            {
                global $current_user;
                $posts[0]->post_content = '<div class="widthlimit">[credit_search]<h1 class="underline">Problem detected</h1><h2>'.$e->getMessage().'</h2><p>IT happens. <span style="float:right"><a href="result/'.$reg.'/"><b>Back</b></a></span></p>';
                //if($e->getCode() == 0) $posts[0]->post_content .= '<form name="email" action="resend" method="post"><input type="hidden" name="act" value="resend"><input type="hidden" name="resend_reg" value="'.$reg.'"><input type="hidden" name="resend_mail" value="'.$current_user->user_email.'"><input type="submit" class="buy" value="Tell us about it"></form>';
                $posts[0]->post_content .= '</div>';
                return $posts;
            }
            if (!$result) self::redirect_page();
            $filename = self::save_report($result, $obj);
            wp_redirect(get_home_url().'/report/' . $filename);
            exit;
        } else {
            $posts[0]->post_content = '<div class="space center"><div class="widthlimit">[credit_search]</div></div>';
        }

        if (isset($_POST['ss'])) $posts[0]->post_content = self::get_report_search_content(sanitize_text_field($_POST['ss']));

        if (get_query_var('act') == 'result' && get_query_var('reg')) $posts[0]->post_content = self::get_report_result_content(sanitize_text_field(get_query_var('reg')));

        return $posts;
    }


    /**
     * Get report result page content
     * @return string
     */
    public static function get_report_result_content($reg)
    {
        self::init_soap();
        try{$object = self::$soap->getFreeInfo('EST', $reg, wp_get_current_user()->user_email)->asObject();} catch(Exception $e){self::redirect_page();}
        $html = '<div class="widthlimit">';
        $html .= '[credit_search]';
        if(get_user_meta(wp_get_current_user()->ID,'permission',true) != 1 )  return $html .= '</br>'. __('Access to search is restricted.', 'bisnode_creditreport'). '</div>';
        $html .= '<div class="two columns">';
        $html .= '<h1>' . $object->general->company_name . '</h1>';
        $html .= '<dl>';

        if (isset($object->old_names->item) && count($object->old_names->item))
        :
            $old_names = self::fix_array($object->old_names);
            $html .= '<dt>'.__('Previous name(s)', 'bisnode_creditreport').':</dt><dd>';
            foreach ($old_names as $item) $html .= $item . '<br/>';
        endif;
        $link = self::$url;
        $html .= '</dd>';

        $l = urlencode($object->general->address);
        $report = $object->last_annual_report ? $object->last_annual_report : 'none';

        $info_general = array(
            'company_type' => __('Company type', 'bisnode_creditreport'),
            'registration_number' => __('Registration number', 'bisnode_creditreport'),
            'vat_number' => __('VAT number', 'bisnode_creditreport'),
            'status' => __('Status', 'bisnode_creditreport')
        );
	$c_type = $object->general->company_type_code;

        foreach($info_general as $k=>$v)
        {
            $item = $object->general->$k ? $object->general->$k : '-';
            $html.= "<dt>$v</dt><dd>$item</dd>";
        }
        $remarks = $object->debts_rating->status ? $object->debts_rating->status : '-';
        $remarks_txt = __('Payment remarks', 'bisnode_creditreport');
        $annual_report_txt = __('Latest annual report', 'bisnode_creditreport');
        $html .= <<< EOT
        <dt>$remarks_txt:</dt><dd><strong>{$remarks}</strong></dd>
        <dt>$annual_report_txt:</dt><dd>{$report}</dd>
        </dl>
        <dl>
EOT;

        $info_contacts = array(
            'Address' => __('Address', 'bisnode_creditreport'),
            'Telephone' => __('Telephone', 'bisnode_creditreport'),
            'E-mail address' => __('E-mail address', 'bisnode_creditreport'),
            'Internet address (www)' => __('Internet address (www)', 'bisnode_creditreport')
        );
        if($items = self::fix_array($object->contacts)):
            foreach($items as $item)
            {
                $type = isset($info_contacts[$item->type]) ? $info_contacts[$item->type] : $item->type;
                $html .= "<dt>$type</dt><dd>$item->content</dd>";
            }

        endif;

        $html .= <<<EOT
        </dl>
        </div>
        <div class="two columns">
         <br>
EOT;
        if($l)
            $html.= <<<EOT
         <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q={$l}&key=AIzaSyAe6B0rZBFFXeIoe6M8BlAl7VNpQ5qCWjw"></iframe>
EOT;
        $html.=<<<EOT
        </div>
        <div class="clear"></div>
EOT;

        if($reg == 10117826) $html .='<p class="message">'.__('This is free example report.', 'bisnode_creditreport').'</p>';
        $short = __('Get Short Credit Report', 'bisnode_creditreport');
        $full = __('Get Full Credit Report', 'bisnode_creditreport');
        if($c_type != 'RA') $html.= <<< EOT
        <form action="$link" method="post">
        <input type="hidden" name="reg" value="$reg">
        <input type="submit" name="report_type" class="buy" value="$full" style="max-width:430px; display:inline-block; vertical-align:top;">
<div style="display:inline-block; max-width:430px; margin-left:0.3em;">
        <p style="color:grey; font-size:12px;">Note: Short Credit Report is off production since 06.04.2016. You're welcome to use Full Credit Report which includes much more detailed information and more accurate AAA credit rating system. Price of Full Report is same as Short Report was. If you have any questions do not hesitate to contact us: info@bisnode.ee.</p>
</div>  
      </form>
EOT;

        $html .= '</div>';
        return $html;
    }

    /**
     * Get report search page content
     * @return string
     */
    public static function get_report_search_content($search)
    {
        //$url = 'https://ariregister.rik.ee/autocomplete.py?nimi=' . $search;
        //$response = wp_remote_get($url);
        //try{$result = json_decode($response['body']);}catch(Exception $e){return 'Some error';}
        self::init_soap();      
        
        try{$result = self::$soap->searchCompany('EST', $search)->asObject();} catch(Exception $e){self::redirect_page();}
        $result = self::fix_array($result);
        $html = '<div class="widthlimit">';
        $html .= '[credit_search]';
        $html .= '<h1 class="underline">'.count($result). ' '. __("RESULTS FOUND", "bisnode_creditreport").'</h1>';

        if (!$result) $html .= '</br>Sorry, nothing found';
        elseif(get_user_meta(wp_get_current_user()->ID,'permission',true) != 1 )  $html .= '</br>'. __('Access to search is restricted.', 'bisnode_creditreport');
        else{
            $html .='<ul id="search_result" class="zebra">';
            foreach ($result as $res) {
                $nimi = strtoupper($res->business_name);
                $html .= '<li>';
                $html .= "<a href='result/" . $res->registration_number . "/'><i>$res->registration_number</i>$nimi</a>";
            }
            $html .= '</li>';
            $html .= '</ul>';
        }
        $html .= '</div>';
        return $html;
    }

    public static function redirect_page()
    {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . get_bloginfo('url'));
        exit();
    }

    /**
     * Save report info into db and put pdf into server dir
     * @param $result
     * @param $obj
     * @return string
     */
    public function save_report($result, $obj)
    {
        $filename_pdf = basename($result->report->pdf, '.pdf');

        $user = wp_get_current_user();

        $post_data = array (
            'comment_status'    => 'closed',
            'ping_status'       => 'closed',
            'post_author'       => $user->user_email,
            'post_name'         => $filename_pdf,
            'post_title'        => $result->general->company_name,
            'post_status'       => 'publish',
            'post_type'         => 'report',
            'post_content'      => file_get_contents($result->report->html),

        );

        $args = array(
            'post_type'=> 'report',
            'name'    =>  $filename_pdf,
        );
        $q = query_posts( $args );
        $res = isset($q[0]) ? $q[0] : false;
        if(($res && $res->post_name != $filename_pdf) || !$res)
        {
            kses_remove_filters(); //remove filters (sanitize)
            $post_ID = wp_insert_post( $post_data );
            kses_init_filters(); //add filters (sanitize)
            update_post_meta( $post_ID, 'reg_code', $result->general->registration_number);
            update_post_meta( $post_ID, 'language', $obj->language);
            update_post_meta( $post_ID, 'download_pdf_link', $result->report->pdf);
            update_post_meta( $post_ID, 'download_html_link', $result->report->html);
            update_post_meta( $post_ID, 'type', $obj->type);
            update_post_meta( $post_ID, 'customer', $user->user_login);
        }


        return $filename_pdf;
    }



    /**
     * Download credit report
     * @param $report_id
     */
    public function download_report($hash)
    {
        wp_redirect("https:/in.bisnode.ee/soap/dl/$hash.pdf", 301);
        exit;
    }

    /**
     * Shortcode for credit report search
     * @param $form
     * @return string
     */
    public static function credit_search()
    {
        $txt = __('Find a company', 'bisnode_creditreport');
        $form = '
        <form id="search_main" action="' . self::$url . '" method="post">
        <input id = "ss" placeholder="'.$txt.'..." type="text" name="ss" value="' . get_search_query() . '" />
        <input type="submit" id="search_submit" value="Search" />
        </form>
        ';

        return $form;
    }

    /**
     * Shortcode for report-list
     * @param $form
     * @return string
     */
    public static function report_list()
    {
        $user = wp_get_current_user();

        $html = '<div class="widthlimit">';
        $html.= '<h1 class="underline">'. __('User information', 'bisnode_creditreport').'</h1><dl>';

        foreach (array(
                     'user_email'=>__('E-mail', 'bisnode_creditreport'),
                     'user_login'=>__('Name', 'bisnode_creditreport'),) as $k=>$v):
            $html.="<dt>$v</dt>";
            $html.="<dd>". $user->$k ."</dd>";
        endforeach;

        $html .='</dl><div class="clear"></div><h1 class="underline">'. __('My reports', 'bisnode_creditreport').'</h1>';
        if(get_user_meta(wp_get_current_user()->ID,'permission',true) != 1 )  return $html .= '</br>'.__('Access to search is restricted.', 'bisnode_creditreport');

        $args = array(
            'post_type'=> 'report',
            'meta_key' => 'customer',
            'meta_value' =>  wp_get_current_user()->user_login

        );

        $q = new WP_Query( $args );

        $q = $q->posts;
        $report_types = array(
            'Short' => __('Short', 'bisnode_creditreport'),
            'Full' => __('Full', 'bisnode_creditreport')
        );

        if (!$q || (isset($q[0]) && $q[0]->ID == 0)) $html .= '<h2>'. __('Nothing found', 'bisnode_creditreport') .'</h2>';
        else {
            $html .= '<h2>'.count($q). ' ' . __('reports found', 'bisnode_creditreport').'</h2>';
            $html .= '<ul id="purchases_list" class="zebra">';
            foreach ($q as $report) :
                $pdf = get_post_meta($report->ID, 'download_pdf_link');
                $reg = get_post_meta($report->ID, 'reg_code');
                $type = get_post_meta($report->ID, 'type');
                $lang = get_post_meta($report->ID, 'language');
                $hash = basename($pdf[0], '.pdf');

                $html .= '<li>';
                $html .= '<a href="'.get_home_url().'/report/'.$hash.'">';
                $html .= '<span class="date">' . date("d/m/Y", strtotime($report->post_modified)) . '</span>';
                $html .= '<span class="reg">' . $reg[0] . '</span>';
                $html .= '<span class="type">' . $report_types[ucfirst($type[0])] . '</span>';
                $html .= '<span class="lang">' . $lang[0] . '</span>';
                $html .= '<span class="name">' . $report->post_title . '</span>';
                $html .= '</a>';
                $html .= '</li>';
            endforeach;
        }

        $html .= '</ul>';

        return $html;
    }

    public static function fix_array ($obj) {
        if (!isset($obj->item)) return array();
        return is_array($obj->item) ? $obj->item : array($obj->item );
    }

    public static function bisnodeCall($reg_code){
        return;
        if(!current_user_can( 'manage_options' ) && self::$ticket)
        {
            $subject = "$reg_code getCreditReport";
            $res = Bisnode_Finland::getInstance()->bill(self::$ticket, $subject);
        }
    }
}
