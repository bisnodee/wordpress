<?php

Class Bisnode_Authorisator
{

    private static $initiated = false;

    /**
     * @var $fin Bisnode_Finland
     */
    private static $fin, $ticket;

    public static function init()
    {
        if (!self::$fin) self::$fin = Bisnode_Finland::getInstance();
        if (!self::$initiated) {
            self::init_hooks();
        }
        if(!session_id())
            session_start();
    }


    private static function init_hooks()
    {

        self::$initiated = true;
        add_action('wp_authenticate', array('Bisnode_Authorisator', 'check_permission'), 1, 2);
        add_filter('wp_logout', array('Bisnode_Authorisator', 'logout_session'));
    }


    /**
     * Checking permission from Finland
     * @param $username
     * @param $password
     */
    public static function check_permission($username, $password)
    {

        $result = self::$fin->getPermissionTicket($username, $password);

        if(!$ticket = trim($result->Ticket)) $permission = false;

        else
        {
            $right = self::$fin->getPermissionUserRightLevel($ticket, 'Service', 'CreditReportEstonia');
            $permission = trim($right->UserRightLevel) == 'READ' ? true : false;
        }

        if($permission)
        {
            self::$ticket = $ticket;
            add_filter( 'authenticate', array('Bisnode_Authorisator', 'rotary_email_login_authenticate'), 20, 3 );
        }
        else add_filter( 'authenticate', 'wp_authenticate_username_password', 20, 3 );
    }

    /**
     * destroy session while logout
     */
    public static function logout_session(){
        session_destroy();
    }


    /**
     * Login as bisnode_finland
     * @return WP_User
     */
    public static function rotary_email_login_authenticate($user, $username, $password) {

        $_SESSION['auth'] = array(
            'ticket' => self::$ticket,
            'permission' => true,
            'login' => $username
        );

        return get_user_by('login', 'bisnode_finland');
    }

    /**
     * Delete dummy user after plugin deactivation
     */
    public static function plugin_deactivation(){
        $user = get_user_by('login', 'bisnode_finland');
        wp_delete_user($user->id);
    }

    /**
     * Create dummy user for auth users.
     */
    public static function plugin_activation(){

        $length = 10;
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        wp_create_user('bisnode_finland', $randomString, null);

    }


}