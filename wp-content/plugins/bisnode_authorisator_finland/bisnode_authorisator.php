<?php
/*
Plugin Name: Bisnode authorisator FINLAND
Plugin URI: http://www.bisnode.ee
Description: Bisnode credit report provides information about more then 500 000 companies using soap requests. Feel free to search and get whole information you need. To get started: 1) Click the "Activate" link to the left of this description, 2) Go to your Credit report settings page, and put your token.
Version: 1.0
Author: Bisnode Estonia AS
License: Bisnode Estonia AS
*/

define('BISNODE_AUTHORISATOR__PLUGIN_DIR', plugin_dir_path(__FILE__));
require_once(BISNODE_AUTHORISATOR__PLUGIN_DIR . 'class.bisnode_authorisator.php');
require_once(BISNODE_AUTHORISATOR__PLUGIN_DIR . 'Bisnode_Finland.class.php');

register_activation_hook(__FILE__, array('Bisnode_Authorisator', 'plugin_activation'));
register_deactivation_hook(__FILE__, array('Bisnode_Authorisator', 'plugin_deactivation'));


add_action('init', array('Bisnode_Authorisator', 'init'));