<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Bisnode_Finland
{

    private $client    = null,
        $debug     = false
    ;

    private static $instance = null;

    /**
     * Initializing is forbidden, this class is Singletone
     * @see BisnodeFinland::getInstance()
     */
    private function __construct()
    {
    }

    /**
     * Get instance of Bisnode client
     * @return Bisnode_Finland
     */
    public static function getInstance()
    {
        return is_null(self::$instance)
            ? self::$instance = new self()
            : self::$instance;
    }



    /**
     * Authorize and get profile information
     * Example:
    <Token>
    <Endusername>test.user@bisnode.ee</Endusername>
    <Exception i:nil="true"/>
    <IpAddress>172.25.172.36;213.219.99.194:53800</IpAddress>
    <LanguageCode>en</LanguageCode>
    <PasswordExpired>false</PasswordExpired>
    <PasswordExpires>2014-08-15T16:21:57</PasswordExpires>
    <Ticket>2050XFBB97BB1BAF4A72C51FD2F8D253C5ED8205F03CE</Ticket>
    <Username/>
    </Token>
     * The end-user login t othe serveiceservices, ie Ticket Search:
     * @see https://www.bisnode.fi/WEB.API/Permission/Ticket?endusername=test.user%40bisnode.ee&password=eestitest
     * @return SimpleXMLElement
     */
    public function getPermissionTicket($username, $password)
    {
        $query = http_build_query(array('endusername'=>$username, 'password'=>$password));
        $url = "https://www.bisnode.fi/WEB.API/Permission/Ticket?".$query;
        return $this->_request($url);
    }


    /**
     * Authorize and get profile information
     * Example:
     * <RightLevel>
     *   <Exception i:nil="true"/>
     *   <UserRightLevel>READ</UserRightLevel>
     * </RightLevel>   *
     * The end-user login t othe serveiceservices, ie Ticket Search:
     * @see https://www.bisnode.fi/WEB.API/Permission/UserRightLevel?applicationname=CreditReport%20Estonia&ticket=2050XFBB97BB1BAF4A72C51FD2F8D253C5ED8205F03CE&righttype=Service&rightitempath=CreditReportEstonia
     * @return SimpleXMLElement
     */
    public function getPermissionUserRightLevel($ticket, $righttype='Service', $rightitempath = 'CreditReportEstonia' )
    {
        $query = http_build_query(array(
            'applicationname' => 'CreditReport Estonia',
            'ticket'=>$ticket,
            'righttype'=>$righttype,
            'rightitempath'=>$rightitempath,
        ));
        $url = "https://www.bisnode.fi/WEB.API/Permission/UserRightLevel?".$query;
        return $this->_request($url);
    }

    /**
     * Request API with post or without
     * @param string $url
     * @param array|string $post
     * @param callback $wrap
     * @return SimpleXMLElement or wrapped/raw data
     */
    private function _request( $url, $post = null )
    {
        if (is_null($this->client)) $this->_curl_init ();
        curl_setopt($this->client, CURLOPT_URL, $url);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->client, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($this->client, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->client, CURLOPT_HTTPHEADER, array(
            "Accept: application/xml",
        ));
        $data = curl_exec($this->client);
        return new SimpleXMLElement($data);
    }


    private function _curl_init()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $this->client = $ch;
    }

    /**
     * Authorize and get profile information
     * Example:
    <Token>
    <Endusername>test.user@bisnode.ee</Endusername>
    <Exception i:nil="true"/>
    <IpAddress>172.25.172.36;213.219.99.194:53800</IpAddress>
    <LanguageCode>en</LanguageCode>
    <PasswordExpired>false</PasswordExpired>
    <PasswordExpires>2014-08-15T16:21:57</PasswordExpires>
    <Ticket>2050XFBB97BB1BAF4A72C51FD2F8D253C5ED8205F03CE</Ticket>
    <Username/>
    </Token>
     * The end-user login t othe serveiceservices, ie Ticket Search:
     * @see https://www.bisnode.fi/WEB.API/Permission/Ticket?endusername=test.user%40bisnode.ee&password=eestitest
     * @return SimpleXMLElement
     */
    public function bill($ticket, $reference)
    {
        $query = http_build_query(array(
            'applicationname'=>'CreditReport Estonia',
            'righttype'=>'Service',
            'rightitempath'=>'CreditReportEstonia',
            'productid'=>59,
            'reference'=>$reference,
            'itemcount'=>1,
            'ticket'=>$ticket,
        ));
        $url = "https://www.bisnode.fi/web.api.proxy/bill?".$query;
        return $this->_request($url);
    }

}