<?php

Class Bisnode_Authorisator_estonia
{

    private static $initiated = false;

    /**
     * @var $fin Bisnode_Finland
     */
    private static $est, $user;

    public static function init()
    {
        if (!self::$est) self::$est = Bisnode_Soap_Client::getInstance();
        if (!self::$initiated) {
            self::init_hooks();
        }
    }


    private static function init_hooks()
    {

        self::$initiated = true;
        add_action('wp_authenticate', array('Bisnode_Authorisator_estonia', 'check_permission'), 1, 2);
        add_filter('wp_logout', array('Bisnode_Authorisator_estonia', 'logout_session'));
    }


    /**
     * Checking permission from Finland
     * @param $username
     * @param $password
     */
    public static function check_permission($username, $password)
    {

        try{$result = self::$est->getProfile($username, $password); }
        catch(Exception $e){$permission = false; }

        if(isset($result))
        {
            self::$user = $result->obj;
            add_filter( 'authenticate', array('Bisnode_Authorisator_estonia', 'rotary_email_login_authenticate'), 20, 3 );
        }
        else add_filter( 'authenticate', 'wp_authenticate_username_password', 20, 3 );
    }

    /**
     * destroy session while logout
     */
    public static function logout_session(){
    }


    /**
     * Login as bisnode_finland
     * @return WP_User
     */
    public static function rotary_email_login_authenticate($user, $username, $password) {

        $session = WP_Session::get_instance();
        $session['bisnode_auth'] = array(
            'permission' => true,
            'user' => self::$user
        );

        return get_user_by('login', 'bisnode_estonia');
    }

    /**
     * Delete dummy user after plugin deactivation
     */
    public static function plugin_deactivation(){
        $user = get_user_by('login', 'bisnode_estonia');
        wp_delete_user($user->id);
    }

    /**
     * Create dummy user for auth users.
     */
    public static function plugin_activation(){

        $length = 10;
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        wp_create_user('bisnode_estonia', $randomString, null);

    }


}