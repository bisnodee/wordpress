<?php get_header(); ?>
<section class="frontpage">
	<?php $pics = array(
		'images/Image2.jpg',
		'images/Image3.png',
		'images/Image5.jpg',
		'images/Image9.jpg',
	); shuffle($pics);?>

	<style type="text/css">
		.frontpage {
			background-image: url(<?php echo bloginfo('stylesheet_directory').'/'.$pics[0] ?>);
		}
	</style>
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>
