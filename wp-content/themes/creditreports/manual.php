<?php
/**
 * Template Name: Manual
 *
 * @package Bisnode
 * @subpackage Bisnode
 * @since Bisnode
 */
?>


<?php get_header(); ?>
<section class="contentpage">
<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <div class="widthlimit">

        <style>
            .c3 { text-align: center; }
            .b { font-weight: bold; }

            .c12 { width: 12em; }
            .c20 { width: 20em; }

            h2 { margin: 2em 0 0; }
        </style>
        <h1 class="underline">BISNODE CREDIT RATING - MANUAL</h1>

        <p class="c8 c9"><span class="c18">Bisnode credit rating is gives you quick assessment
of company's creditworthiness. For calculation of credit rating Bisnode takes into
account company's:</span></p>

        <ul>
            <li>status and age</li>
            <li>economic and financial situation</li>
            <li>payment habits</li>
        </ul>

        <h2>RATINGS</h2>

        <p class="c8 c9 c13"></p><a href="#" name=
        "39e0e541f6e60eb9b49eca61a1ddb061893c7563"></a><a href="#" name="0"></a>

        <table cellpadding="0" cellspacing="0" class="c16">
            <tbody>
            <tr class="c14">
                <td class="c3">
                    <p>
                        <img alt="Rating-Positive.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image00.jpg"
                             title="" /></p>
                    <p class="c11 c9"><span class="c2 b">Low credit risk</span></p>
                    <p class="c11 c9"><span class="c2">Credit recommended</span></p>
                </td>

                <td class="c3">
                    <p>
                        <img alt="Rating-Passable.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image12.jpg"
                             title="" /></p>
                    <p class="c9 c11"><span class="c2 b">Moderate credit risk</span></p>
                    <p class="c11 c9"><span class="c2">Credit not recommended</span></p>
                </td>

                <td class="c3">
                    <p>
                        <img alt="Rating-Negative.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image09.jpg"
                             title="" /></p>
                    <p class="c11 c9"><span class="c2 b">High credit risk</span></p>
                    <p class="c11 c9"><span class="c2">Credit not recommended</span></p>
                </td>

                <td class="c3">
                    <p>
                        <img alt="Rating-NoRating.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image06.jpg"
                             title="" /></p>
                    <p class="c11 c9"><span class="c2 b">No rating*</span></p>
                    <p class="c11 c9"><span class="c2">Credit not recommended</span></p>
                </td>
            </tr>

            <tr class="c14">
                <td class="c3">
                </td>

                <td class="c3">

                </td>

                <td class="c3">

                </td>

                <td class="c3">

                </td>
            </tr>
            </tbody>
        </table>

        <p class="c8 c9 c13"></p>

        <p class="c8 c9"><span class="c18">* In case of</span> <span class="c10">No
rating</span><span class="c18">&nbsp;there's not enough information to calculate credit
rating because financial statement or other vital information is not
available.</span></p>

        <p class="c8 c9 c13"></p>

        <p class="c8 c9 c13"></p>

        <h2>SUGGESTED CREDIT LIMIT</h2>

        <p class="c8 c9 c13"></p>

        <p class="c8 c9"><span class="c18">The credit limit is the recommended limit amount for
selling on credit. Recommended limit is</span> <span class="c10">3% of net sales or 3%
of equity total</span><span class="c18">&nbsp;of the company. Credit is only
recommended to companies with</span> <span class="c10">Positive</span><span class=
                                                                            "c18">&nbsp;credit rating.</span></p>

        <p class="c8 c9 c13"></p>

        <p class="c8 c9 c13"></p>

        <h2>FINANCIAL RATIOS</h2>

        <p class="c8 c9 c13"></p><a href="#" name=
        "4d149e3db66e072cbed4c67a1cfbd7686c9c42af"></a><a href="#" name="1"></a>

        <table cellpadding="0" cellspacing="0" class="c1">
            <tbody>
            <tr class="c14">
                <td class="c20">
                    <p class="c7"><span class="c6 b">Return on assets %</span></p>
                </td>

                <td class="c12">
                    <p class="c7"><span>
<img alt="Green-VeryGood.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image11.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7"><span class="c2">15,0% and more</span></p>
                </td>
            </tr>

            <tr class="c22">
                <td class="c12">
                    <p class="c7 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7"><span>
<img alt="Green-Good.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image02.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7"><span class="c2">5,0% - 14,9%</span></p>
                </td>
            </tr>

            <tr class="c14">
                <td class="c12">
                    <p class="c7 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7"><span>
<img alt="Yellow-Passable.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image05.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7"><span class="c2">1,0% - 4,9 %</span></p>
                </td>
            </tr>

            <tr class="c14">
                <td class="c12">
                    <p class="c7 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7"><span>
<img alt="Orange-Negative.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image01.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7"><span class="c2">less than 1,0%</span></p>
                </td>
            </tr>
            </tbody>
        </table>

        <p class="c8 c9"><span class="c4">Return on assets</span><span class=
                                                                       "c15 c21 c17">&nbsp;- an indicator of how profitable a company is relative to its total
assets. ROA gives an idea as to how efficient management is at using its assets to
generate earnings. Calculated by dividing a company's annual earnings by its total
assets, ROA is displayed as a percentage.</span></p>

        <p class="c8 c9 c13"></p><a href="#" name="ea7ffdcb26b255c96002a5dbdcc7e6c59658fb9a"
                                    id="ea7ffdcb26b255c96002a5dbdcc7e6c59658fb9a"></a><a href="#" name="2"></a>

        <table cellpadding="0" cellspacing="0" class="c1">
            <tbody>
            <tr class="c14">
                <td class="c20">
                    <p class="c7 c8"><span class="c6 b">Equity ratio</span></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Green-Excellent.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image07.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">50% or more</span></p>
                </td>
            </tr>

            <tr class="c14">
                <td class="c12">
                    <p class="c7 c8 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Green-VeryGood.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image11.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">30,0% - 49,9%</span></p>
                </td>
            </tr>

            <tr class="c22">
                <td class="c12">
                    <p class="c7 c8 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Green-Good.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image02.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">10,0% - 29,9%</span></p>
                </td>
            </tr>

            <tr class="c14">
                <td class="c12">
                    <p class="c7 c8 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Yellow-Passable.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image05.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">0,0% - 9,9%</span></p>
                </td>
            </tr>

            <tr class="c14">
                <td class="c12">
                    <p class="c7 c8 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Orange-Negative.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image01.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">less than 0,0 %</span></p>
                </td>
            </tr>
            </tbody>
        </table>

        <p class="c8 c9"><span class="c4">Equity ratio</span> <span class="c15 c21 c17">- a
measure of a company's financial leverage calculated by dividing its total liabilities
by stockholders' equity. It indicates what proportion of equity and debt the company is
using to finance its assets.</span></p>

        <p class="c8 c9 c13"></p><a href="#" name=
        "628d175c2020d90a2b9aa2f883412d8b5a226c1b"></a><a href="#" name="3"></a>

        <table cellpadding="0" cellspacing="0" class="c16">
            <tbody>
            <tr class="c14">
                <td class="c20">
                    <p class="c7 c8"><span class="c6 b">Quick ratio</span></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Green-Good.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image02.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">more than 0,8</span></p>
                </td>
            </tr>

            <tr class="c14">
                <td class="c12">
                    <p class="c7 c8 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Yellow-Passable.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image05.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">0,6 - 0,8</span></p>
                </td>
            </tr>

            <tr class="c22">
                <td class="c12">
                    <p class="c7 c8 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Orange-Poor.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image08.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">less than 0,6</span></p>
                </td>
            </tr>
            </tbody>
        </table>

        <p class="c8 c9"><span class="c4">Quick ratio</span><span class="c15 c17 c21">&nbsp;-
an indicator of a company&rsquo;s short-term liquidity. The quick ratio measures a
company&rsquo;s ability to meet its short-term obligations with its most liquid
assets.</span></p>

        <p class="c8 c9 c13"></p>

        <p class="c8 c9 c13"></p>

        <h2>DEBT COLLECTION CASES AND TAX ARREARS</h2>

        <p class="c8 c9 c13"></p><a href="#" name=
        "938e08434054192e15eada982296ac98b55037f7"></a><a href="#" name="4"></a>

        <table cellpadding="0" cellspacing="0" class="c1">
            <tbody>
            <tr class="c14">
                <td class="c20">
                    <p class="c7 c8"><span class="c6 b">Debt collection cases</span></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Green-None.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image10.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">No open debt collection cases</span></p>
                </td>
            </tr>

            <tr class="c14">
                <td class="c12">
                    <p class="c7 c8 c13"></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Orange-Exist.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image03.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">Debt collection cases exist</span></p>
                </td>
            </tr>
            </tbody>
        </table>

        <p class="c8 c9"><span class="c15 c17">Data source: major debt collection agencies of
Estonia. Information is updated daily.</span></p>

        <p class="c8 c9 c13"></p><a href="#" name="bc305c6c6ec14d50f379afadc707ae8b67980a1a"
                                    id="bc305c6c6ec14d50f379afadc707ae8b67980a1a"></a><a href="#" name="5"></a>

        <table cellpadding="0" cellspacing="0" class="c1">
            <tbody>
            <tr class="c14">
                <td class="c20">
                    <p class="c7 c8"><span class="c6 b">Tax arrears</span></p>
                </td>

                <td class="c12">
                    <p class="c7 c8"><span>
<img alt="Green-None.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image10.jpg"
     title="" /></span></p>
                </td>

                <td class="c20">
                    <p class="c7 c8"><span class="c2">No tax arrears</span></p>
                </td>
            </tr>

            <tr class="c14">
                <td class="c12">
                    <p class="c7 c8 c13"></p>
                </td>

                <td class="c19">
                    <p class="c7 c8"><span>
<img alt="Yellow-Minor.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image04.jpg"
     title="" /></span></p>
                </td>

                <td class="c0">
                    <p class="c7 c8"><span class="c2">Minor tax arrears (&lt;100 EUR)</span></p>
                </td>
            </tr>

            <tr class="c22">
                <td class="c12">
                    <p class="c7 c8 c13"></p>
                </td>

                <td class="c19">
                    <p class="c7 c8"><span>
<img alt="Orange-Exist.jpg" src="<?php bloginfo('template_directory'); ?>/images/manual/image03.jpg"
     title="" /></span></p>
                </td>

                <td class="c0">
                    <p class="c7 c8"><span class="c2">Tax arrears exist (&gt;100 EUR)</span></p>
                </td>
            </tr>
            </tbody>
        </table>

        <p class="c8 c9"><span class="c15 c17">Information on Tax Arrears is based on live
query to Estonian Tax and Customs Board and historical data.</span></p> </div>
<?php endwhile; endif; ?>
    </div>
<?php get_footer(); ?>