<?php
/**
* Template Name: Example
*
* @package Bisnode
* @subpackage Bisnode
* @since Bisnode
*/
?>

<?php get_header();
$type = isset($_GET['show']) ? $_GET['show'] : 'short'?>
<section class="contentpage">
<?php

$args = array(
    'post_type'=> 'report',
    'meta_query' => array(
        array(
            'key' => 'reg_code',
            'value' => 10117826,
        ),
        array(
            'key' => 'type',
            'value' => $type,
        )));

$q = new WP_Query( $args );
$q = reset($q->posts);
$link = get_post_meta($q->ID, 'download_pdf_link');
if($link) {echo '<div class="widthlimit"><br/>'.$q->post_content.'<a class="buy" href="' .$link[0]. '">'.__('Download PDF', 'creditreports').'</a></div>';}
?>
</section>
<?php get_footer(); ?>




