<?php
/**
 * Template Name: Report
 *
 * @package Bisnode
 * @subpackage Bisnode
 * @since Bisnode
 */
?>

<?php get_header(); ?>
<section class="contentpage">
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>
    </div>
    <?php get_footer(); ?>



