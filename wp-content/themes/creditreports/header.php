<!doctype html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script>
		$(document).bind("mobileinit", function(){
			$.mobile.defaultPageTransition = 'slide';
		});
		$("#testBt").bind("vclick",function(){
			$("#menu-menu").slideToggle();
		});

		$(function(){
			$('.zebra > *').filter(':even').addClass('even');
			$('.zebra > *').filter(':odd').addClass('odd');


			$(window).resize(function(){

				var wh =  $(this).height();
				var h1 = Math.round( wh / 4 ) +'px';
				var h2 = Math.round( wh / 2 ) +'px';
				$('#content').css({minHeight: h2});

				$('#menu-menu, #menu-menu li').removeAttr( 'style' );

			}).resize();

			$('#menu-toggle').click(function(){
				$('#menu-menu').fadeToggle();
				$('#menu-menu li').slideToggle();
				$(this).blur();
				return false;
			});

			$('fieldset').wrap('<div class="hscroll"></div>');

		});
	</script>
	<?php wp_head(); ?>
</head>
<body <?php  body_class(!is_user_logged_in() ? 'logged-out' : null); ?>>
	<div class="wrapper">
	<header class="clearfix" id="header">
    	<div class="widthlimit"><a href="/">
				<?php /* <img src="<?php echo get_bloginfo('template_directory');?>/images/logo-creditreports.png"  id="logo_id_big"> */ ?>
				<img src="<?php echo get_bloginfo('template_directory');?>/images/logo-bisnode.png"  id="logo_id_small">
			</a>
			<a href="#menu-toggle" id="menu-toggle">
				<img src="<?php echo get_bloginfo('template_directory');?>/images/icon_menu_black.png"></a><div id="lang"><?php echo qtranxf_generateLanguageSelectCode('image'); ?></div>
                <nav class="main-navigation">
					<?php wp_nav_menu(array('menu' => 'top-menu', 'menu_class' => 'top-menu'));?>
	  			</nav>
         </div>
	</header>
