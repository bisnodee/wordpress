<?php
/**
* Template Name: Full Width login
*
* @package Bisnode
* @subpackage Bisnode
* @since Bisnode
* Text Domain: creditreport
*/
?>
<?php if(is_user_logged_in()) {wp_redirect( home_url() ); exit;}?>
<?php
$login_txt = __('Username', 'creditreports');
$login_email_txt = __('Username/e-mail', 'creditreports');
$pass_txt = __('Password', 'creditreports');
$email_txt = __('E-mail', 'creditreports');
?>
<?php

$action = isset($_GET['action']) ? $_GET['action'] : null;

if($action == 'rp'){
    $user = check_password_reset_key( isset($_GET['key']) ? $_GET['key'] : null, isset($_GET['login']) ? $_GET['login'] : null );
    if(isset($user->errors))
    {
        $errors = new WP_Error();
        $errors->add('wrong key', __('Wrong key or login'));
    }
}
if($action == 'resetpass' && isset($_POST['rp_key']) && isset($_POST['rp_login'])){

    $user = check_password_reset_key( $_POST['rp_key'], $_POST['rp_login'] );
    if(isset($user->errors))
    {
        $errors = new WP_Error();
        $errors->add('wrong key', __('Wrong key or login'));
    }

    if (isset($_POST['pass1']) && $_POST['pass1'] != $_POST['pass2'])
    {
        $errors = new WP_Error();
        $errors->add('password_reset_mismatch', __('The passwords do not match.'));
    }

    if (isset($_POST['pass1']) && strlen($_POST['pass1']) < 4)
    {
        $errors = new WP_Error();
        $errors->add('password_reset_mismatch', __('The password is too short.'));
    }

    if ((!isset($errors)) && isset($_POST['pass1']) && !empty($_POST['pass1'])) :
       reset_password($user, $_POST['pass1']);
       wp_redirect('/login/?action=success');
       exit;
    endif;
}

if($action == 'logout') {
    check_admin_referer('log-out');

    $user = wp_get_current_user();

    wp_logout();

    if (!empty($_REQUEST['redirect_to'])) {
        $redirect_to = $requested_redirect_to = $_REQUEST['redirect_to'];
    } else {
        $redirect_to = 'wp-login.php?loggedout=true';
        $requested_redirect_to = '';
    }

    /**
     * Filter the log out redirect URL.
     *
     * @since 4.2.0
     *
     * @param string $redirect_to The redirect destination URL.
     * @param string $requested_redirect_to The requested redirect destination URL passed as a parameter.
     * @param WP_User $user The WP_User object for the user that's logging out.
     */
    $redirect_to = apply_filters('logout_redirect', $redirect_to, $requested_redirect_to, $user);
    wp_safe_redirect($redirect_to);
    exit();
}
?>


<?php get_header(); ?>

<style type="text/css">
    #register {
        display: none;
    }
</style>

<section class="contentpage">
    <div class="widthlimit">
        <div class="three columns">
            <h1 class="underline"><?php echo __('Log in', 'creditreports')?></h1>
           <?php

           switch($action):

               case 'rp':
                   if(!isset($user->errors)):
                       wp_enqueue_script('utils');
                       //wp_enqueue_script('user-profile');
                       ?>
                       <!-- START RESET FORM -->
                       <span id="r_password">
                           <form name="resetpassform" id="resetpassform" action="<?php echo esc_url( network_site_url( 'login?action=resetpass', 'login_post' ) ); ?>" method="post" autocomplete="off">
                               <input type="hidden" id="user_login" value="<?php echo esc_attr( $_GET['login'] ); ?>" autocomplete="off" />

                               <p class="user-pass1-wrap">
                                   <label for="pass1"><?php _e('Palun sistestage uus parool') ?></label><br />
                               <div class="wp-pwd">
                        <span class="password-input-wrapper">
                            <input type="password" placeholder="<?php echo $pass_txt?>" data-reveal="1" data-pw="" name="pass1" id="pass1" class="input" size="20" value="" autocomplete="off" aria-describedby="pass-strength-result" />
                        </span>
                               </div>
                               </p>
                               <p class="user-pass2-wrap">
                                   <label for="pass2"><?php _e('Confirm new password') ?></label><br />
                                   <input type="password" placeholder="<?php echo $pass_txt?>" name="pass2" id="pass2" class="input" size="20" value="" autocomplete="off" />
                               </p>

                               <?php do_action( 'resetpass_form', $user ); ?>
                               <input type="hidden" name="rp_key" value="<?php echo esc_attr( $_GET['key'] ); ?>" />
                               <input type="hidden" name="rp_login" value="<?php echo esc_attr( $_GET['login'] ); ?>" />
                               <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Uus parool'); ?>" /></p>
                           </form>
                           </span>
                       <!-- END IF RESET FORM -->
                   <?php
                   endif;

               break;

               case 'success':
                   echo '<p>Õnnestus.</p>';
               break;

               case 'forgotpassword':

                   if ( isset( $_GET['error'] ) ) {
                       if ( 'invalidkey' == $_GET['error'] ) {
                           $errors->add( 'invalidkey', __( 'Your password reset link appears to be invalid. Please request a new link below.' ) );
                       } elseif ( 'expiredkey' == $_GET['error'] ) {
                           $errors->add( 'expiredkey', __( 'Your password reset link has expired. Please request a new link below.' ) );
                       }
                   }

                   $lostpassword_redirect = ! empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '';
                   /**
                    * Filter the URL redirected to after submitting the lostpassword/retrievepassword form.
                    *
                    * @since 3.0.0
                    *
                    * @param string $lostpassword_redirect The redirect destination URL.
                    */
                   $redirect_to = apply_filters( 'lostpassword_redirect', $lostpassword_redirect );

                   /**
                    * Fires before the lost password form.
                    *
                    * @since 1.5.1
                    */
                   do_action( 'lost_password' );

             //      login_header(__('Lost Password'), '<p class="message">' . __('Please enter your username or email address. You will receive a link to create a new password via email.') . '</p>', $errors);

                   $user_login = isset($_POST['user_login']) ? wp_unslash($_POST['user_login']) : '';

                   ?>

                   <form name="lostpasswordform" id="lostpasswordform" action="<?php echo esc_url( network_site_url( 'wp-login.php?action=lostpassword', 'login_post' ) ); ?>" method="post">
                       <p>
                           <label for="user_login" ><?php _e('Username or E-mail:', 'creditreports') ?><br />
                               <input type="text" name="user_login" id="user_login" class="input" value="<?php echo esc_attr($user_login); ?>" size="20" /></label>
                       </p>
                       <?php
                       /**
                        * Fires inside the lostpassword form tags, before the hidden fields.
                        *
                        * @since 2.1.0
                        */
                       do_action( 'lostpassword_form' ); ?>
                       <input type="hidden" name="redirect_to" value="<?php echo esc_attr( $redirect_to ); ?>" />
                       <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Get New Password', 'creditreports'); ?>" /></p>
                   </form>


                   <?php
                   break;

           endswitch; ?>

           <!-- START LOGIN FORM -->

            <?php if(isset($_POST['wp-submit']) && isset($_POST['user_login']))
           : echo ' <span id="login">';
               $errors = register_new_user($_POST['user_login'], $_POST['user_email']);
               if ( !is_wp_error($errors) ) : echo '<p class="login-msg" >'.__('Information about resetting your password has been sent. Please check your e-mail.', 'creditreports').'</b></span></p><script type="text/javascript">$("#register").hide();</script>';
               else: echo "<p>".__('Dear customer of Bisnode Estonia, enter your username/e-mail and password or', 'creditreports')." <span id='h_register' style='text-decoration: underline; cursor: pointer'><b>".__('register here', 'creditreports')."</b></span></p>";
               endif;
           else: echo "<span id='login'><p>".__('Dear customer of Bisnode Estonia, enter your username/e-mail and password or', 'creditreports')." <span id='h_register' style='text-decoration: underline; cursor: pointer'><b>".__('register here', 'creditreports')."</b></span></p>";
           endif;


            $login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;

            if ( $login === "failed" ) {
                echo '<p class="login-msg" style="color:red"><strong>'.__('Wrong username or password', 'creditreports').'.<br>'.__('Please try again', 'creditreports').'.</p></strong>';
            } elseif ( $login === "empty" ) {
                echo '<p class="login-msg" style="color:red"><strong>'.__('Wrong username or password', 'creditreports').'.<br>'.__('Please try again', 'creditreports').'.</p></strong>';
            } elseif ( $login === "false" ) {
                echo '<p class="login-msg" style="color:red"><strong>'.__('Wrong username or password', 'creditreports').'.<br>'.__('Please try again', 'creditreports').'.</p></strong>';
            }
            $args = array(
                'redirect' => home_url(),
                'id_username' => 'user',
                'id_password' => 'pass',
                'remember' => false,
                'label_username' => null,
                'label_password' => null,
                'label_log_in' => __('Log in', 'creditreports')
            )
            ;?>
            <b><a href="<?php echo esc_url( network_site_url( 'login?action=forgotpassword', 'login_post' ) ); ?>"><?php echo __('Forgot password', 'creditreports')?>?</a></b>
            <?php bisnode_login_form( $args ); ?>
            </span>

            <!-- END OF LOGIN FORM -->

            <!-- START REGISTER FORM -->
            <span id="register">
                <p id="reg_passmail"><?php _e( 'Confirmation will be sent to you by e-mail.', 'creditreports' ); ?> <span id='h_login' style='text-decoration: underline; cursor: pointer'> <b><?php echo __('Log in', 'creditreports')?></b></span></p>
            <?php
            if (isset($errors) && is_wp_error($errors) ) : echo '<script type="text/javascript">$(\'#login\').hide();$(\'#register\').show();</script>';
                foreach($errors->errors as $error)
                    echo '<p class="login-msg" style="color:red">'.$error[0].'</p>';

            endif;
            ?>


            <form name="registerform" id="loginform" action="<?php echo esc_url( site_url('/login', 'login_post') ); ?>" method="post" novalidate="novalidate">
                <p>
                        <input placeholder="<?php echo $login_txt?>" type="text" name="user_login" id="user_login" class="input" value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20" /></label>
                </p>
                <p>
                        <input placeholder="<?php echo $email_txt;?>" type="email" name="user_email" id="user_email" class="input" value="<?php echo esc_attr( wp_unslash( $user_email ) ); ?>" size="20" /></label>
                </p>
                <?php
                /**
                 * Fires following the 'E-mail' field in the user registration form.
                 *
                 * @since 2.1.0
                 */
                do_action( 'register_form' );
                ?>
                <input type="hidden" name="redirect_to" value="" />
                <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Register', 'creditreports'); ?>" /></p>
            </form>
            </span>

            <!-- END OF REGISTER FORM -->
        </div>

        <div class="three columns double">

            <h1 class="underline"><?php echo __('ABOUT CREDITREPORTS.EE', 'creditreports')?>?</h1>
            <p><?php echo __('The service is currently available for registered customers only.', 'creditreports')?></p>

            <b><?php echo __('Contact us:', 'creditreports')?></b>
            <p>+372 6414 902<br>
                <a href="mailto:info@bisnode.ee">info@bisnode.ee</a></p>

        </div>

        <div class="clear"></div>

        <?php         if(isset($_GET['action']) && $_GET['action'] == 'rp') echo "<script>$('#login').hide();</script>"; ?>
        <?php         if(isset($_GET['action']) && $_GET['action'] == 'forgotpassword') echo "<script>$('#login').hide();</script>"; ?>


        <script>
            $(function() {
                $('#user').attr( 'placeholder', '<?php echo $login_email_txt?>' );
                $('#pass').attr( 'placeholder', '<?php echo $pass_txt?>' );

                $('#h_register').click(function(){
                    $('#r_password').hide();
                    $('#login').hide();
                    $('#register').show();
                });
                $('#h_login').click(function(){
                    $('#r_password').hide();
                    $('#login').show();
                    $('#register').hide();
                });
            });
        </script>

    </div>
</section>
<?php get_footer(); ?>




