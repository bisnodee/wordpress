# Loco Gettext template
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: creditreports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: Fri Nov 20 2015 12:36:16 GMT+0200 (FLE Standard Time)\n"
"POT-Revision-Date: Fri Nov 20 2015 16:02:23 GMT+0200 (FLE Standard Time)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: user <>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Loco-Target-Locale: ee_EE\n"
"X-Generator: Loco - https://localise.biz/"

#. Name of the template
msgid "Example"
msgstr ""

#: /example.php:31
msgid "Download PDF"
msgstr ""

#: /functions.php:89 /page-login.php:99 /page-login.php:226 /page-login.php:237
msgid "Log in"
msgstr ""

#: /functions.php:92
msgid "Log out"
msgstr ""

#: /functions.php:104 /page-login.php:13
msgid "Username"
msgstr ""

#: /functions.php:105 /page-login.php:15
msgid "Password"
msgstr ""

#: /functions.php:106
msgid "Remember Me"
msgstr ""

#: /functions.php:107
msgid "Log In"
msgstr ""

#. Name of the template
msgid "Manual"
msgstr ""

#. Name of the template
msgid "Full Width login"
msgstr ""

#: /page-login.php:14
msgid "Username/e-mail"
msgstr ""

#: /page-login.php:16
msgid "E-mail"
msgstr ""

#: /page-login.php:27 /page-login.php:36
msgid "Wrong key or login"
msgstr ""

#: /page-login.php:42
msgid "The passwords do not match."
msgstr ""

#: /page-login.php:48
msgid "The password is too short."
msgstr ""

#: /page-login.php:115
msgid "Palun sistestage uus parool"
msgstr ""

#: /page-login.php:123
msgid "Confirm new password"
msgstr ""

#: /page-login.php:130
msgid "Uus parool"
msgstr ""

#: /page-login.php:147
msgid ""
"Your password reset link appears to be invalid. Please request a new link "
"below."
msgstr ""

#: /page-login.php:149
msgid "Your password reset link has expired. Please request a new link below."
msgstr ""

#: /page-login.php:178
msgid "Username or E-mail:"
msgstr ""

#: /page-login.php:189
msgid "Get New Password"
msgstr ""

#: /page-login.php:203
msgid ""
"Information about resetting your password has been sent. Please check your e-"
"mail."
msgstr ""

#: /page-login.php:204 /page-login.php:206
msgid "Dear customer of Bisnode Estonia, enter your username/e-mail and password or"
msgstr ""

#: /page-login.php:204 /page-login.php:206
msgid "register here"
msgstr ""

#: /page-login.php:213 /page-login.php:215 /page-login.php:217
msgid "Wrong username or password"
msgstr ""

#: /page-login.php:213 /page-login.php:215 /page-login.php:217
msgid "Please try again"
msgstr ""

#: /page-login.php:229
msgid "Forgot password"
msgstr ""

#: /page-login.php:237
msgid "Confirmation will be sent to you by e-mail."
msgstr ""

#: /page-login.php:263
msgid "Register"
msgstr ""

#: /page-login.php:272
msgid "ABOUT CREDITREPORTS.EE"
msgstr ""

#: /page-login.php:273
msgid "The service is currently available for registered customers only."
msgstr ""

#: /page-login.php:275
msgid "Contact us:"
msgstr ""

#: /page-signup.php:94
msgid "Site Name:"
msgstr ""

#: /page-signup.php:96
msgid "Site Domain:"
msgstr ""

#: /page-signup.php:109
msgid "sitename"
msgstr ""

#: /page-signup.php:111
msgid "domain"
msgstr ""

#: /page-signup.php:112
#, php-format
msgid "Your address will be %s."
msgstr ""

#: /page-signup.php:112
msgid ""
"Must be at least 4 characters, letters and numbers only. It cannot be "
"changed, so choose carefully!"
msgstr ""

#: /page-signup.php:117
msgid "Site Title:"
msgstr ""

#: /page-signup.php:126
msgid "Privacy:"
msgstr ""

#: /page-signup.php:127
msgid "Allow search engines to index this site."
msgstr ""

#: /page-signup.php:131
msgid "Yes"
msgstr ""

#: /page-signup.php:135
msgid "No"
msgstr ""

#: /page-signup.php:177
msgid "Username:"
msgstr ""

#: /page-signup.php:182
msgid "(Must be at least 4 characters, letters and numbers only.)"
msgstr ""

#: /page-signup.php:185
msgid "Email&nbsp;Address:"
msgstr ""

#: /page-signup.php:189
msgid ""
"We send your registration email to this address. (Double-check your email "
"address before continuing.)"
msgstr ""

#: /page-signup.php:256
#, php-format
msgid "Get <em>another</em> %s site in seconds"
msgstr ""

#: /page-signup.php:259
msgid "There was a problem, please correct the form below and try again."
msgstr ""

#: /page-signup.php:262
#, php-format
msgid ""
"Welcome back, %s. By filling out the form below, you can <strong>add another "
"site to your account</strong>. There is no limit to the number of sites you "
"can have, so create to your heart&#8217;s content, but write responsibly!"
msgstr ""

#: /page-signup.php:268
msgid "Sites you are already a member of:"
msgstr ""

#: /page-signup.php:277
msgid ""
"If you&#8217;re not going to use a great site domain, leave it for a new "
"user. Now have at it!"
msgstr ""

#: /page-signup.php:292
msgid "Create Site"
msgstr ""

#: /page-signup.php:374
#, php-format
msgid "The site %s is yours."
msgstr ""

#: /page-signup.php:376
#, php-format
msgid ""
"<a href=\"http://%1$s\">http://%2$s</a> is your new site. <a href=\"%3$s\">Log "
"in</a> as &#8220;%4$s&#8221; using your existing password."
msgstr ""

#: /page-signup.php:430
#, php-format
msgid "Get your own %s account in seconds"
msgstr ""

#: /page-signup.php:446
msgid "Gimme a site!"
msgstr ""

#: /page-signup.php:449
msgid "Just a username, please."
msgstr ""

#: /page-signup.php:453
msgid "Next"
msgstr ""

#: /page-signup.php:498
#, php-format
msgid "%s is your new username"
msgstr ""

#: /page-signup.php:499
msgid ""
"But, before you can start using your new username, <strong>you must activate "
"it</strong>."
msgstr ""

#: /page-signup.php:500 /page-signup.php:629
#, php-format
msgid "Check your inbox at <strong>%s</strong> and click the link given."
msgstr ""

#: /page-signup.php:501
msgid ""
"If you do not activate your username within two days, you will have to sign "
"up again."
msgstr ""

#: /page-signup.php:565
msgid "Signup"
msgstr ""

#: /page-signup.php:626
#, php-format
msgid "Congratulations! Your new site, %s, is almost ready."
msgstr ""

#: /page-signup.php:628
msgid ""
"But, before you can start using your site, <strong>you must activate "
"it</strong>."
msgstr ""

#: /page-signup.php:630
msgid ""
"If you do not activate your site within two days, you will have to sign up "
"again."
msgstr ""

#: /page-signup.php:631
msgid "Still waiting for your email?"
msgstr ""

#: /page-signup.php:633
msgid ""
"If you haven&#8217;t received your email yet, there are a number of things "
"you can do:"
msgstr ""

#: /page-signup.php:635
msgid ""
"Wait a little longer. Sometimes delivery of email can be delayed by "
"processes outside of our control."
msgstr ""

#: /page-signup.php:636
msgid ""
"Check the junk or spam folder of your email client. Sometime emails wind up "
"there by mistake."
msgstr ""

#: /page-signup.php:637
#, php-format
msgid ""
"Have you entered your email correctly? You have entered %s, if it&#8217;s "
"incorrect, you will not receive your email."
msgstr ""

#: /page-signup.php:658
msgctxt "Multisite active signup type"
msgid "all"
msgstr ""

#: /page-signup.php:659
msgctxt "Multisite active signup type"
msgid "none"
msgstr ""

#: /page-signup.php:660
msgctxt "Multisite active signup type"
msgid "blog"
msgstr ""

#: /page-signup.php:661
msgctxt "Multisite active signup type"
msgid "user"
msgstr ""

#: /page-signup.php:664
#, php-format
msgid ""
"Greetings Site Administrator! You are currently allowing &#8220;%s&#8221; "
"registrations. To change or disable registration go to your <a "
"href=\"%s\">Options page</a>."
msgstr ""

#: /page-signup.php:670
msgid "Registration has been disabled."
msgstr ""

#: /page-signup.php:673
#, php-format
msgid "You must first <a href=\"%s\">log in</a>, and then you can create a new site."
msgstr ""

#: /page-signup.php:681
msgid "User registration has been disabled."
msgstr ""

#: /page-signup.php:687
msgid "Site registration has been disabled."
msgstr ""

#: /page-signup.php:706
msgid "Sorry, new registrations are not allowed at this time."
msgstr ""

#: /page-signup.php:708
msgid "You are logged in already. No need to register again!"
msgstr ""

#: /page-signup.php:714
#, php-format
msgid ""
"The site you were looking for, <strong>%s</strong>, does not exist, but you "
"can create it now!"
msgstr ""

#: /page-signup.php:716
#, php-format
msgid "The site you were looking for, <strong>%s</strong>, does not exist."
msgstr ""

#. Name of the template
msgid "Report"
msgstr ""

#. Name of the theme
msgid "creditreports"
msgstr ""

#. Theme URI of the theme
msgid "http://www.creditreports.ee/"
msgstr ""

#. Description of the theme
msgid "creditreports."
msgstr ""

#. Author of the theme
msgid "Anton"
msgstr ""

#. Author URI of the theme
msgid "http://www.freakinc.eu/"
msgstr ""
