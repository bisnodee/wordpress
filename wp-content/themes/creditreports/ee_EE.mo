��          �       |      |     }  +   �     �  L   �          &     -     =  R   N     �     �     �     �     �     �  A   �          %     9     I     d  �  r       4         U  X   g     �     �     �     �  5   �  
   -  
   8     C     O     V     j  \   v     �     �     �  #   	     1	   ABOUT CREDITREPORTS.EE Confirmation will be sent to you by e-mail. Contact us: Dear customer of Bisnode Estonia, enter your username/e-mail and password or Download PDF E-mail Forgot password Get New Password Information about resetting your password has been sent. Please check your e-mail. Log In Log in Log out Password Please try again Register The service is currently available for registered customers only. Username Username or E-mail: Username/e-mail Wrong username or password register here Project-Id-Version: creditreports
Report-Msgid-Bugs-To: 
POT-Creation-Date: Fri Nov 20 2015 12:36:16 GMT+0200 (FLE Standard Time)
PO-Revision-Date: Fri Nov 20 2015 16:27:54 GMT+0200 (FLE Standard Time)
Last-Translator: dmitri <dm.zinovjev@gmail.com>
Language-Team: 
Language: Ewe (Estonia)
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: ee_EE
X-Generator: Loco - https://localise.biz/ PAROOL PUUDUB Registreerimise kinnitus saadetakse Teile e-postiga. Võtke ühendust: Hea Bisnode Estonia klient, sisesta oma kasutajanimi või e-posti aadress ja parool või Salvesta pdf E-post Unustasid parooli Telli uus parool 
Teie parool on saadetud. Palun, kontrollige e-posti. Logi sisse Logi sisse Logi välja Parool Palun proovi uuesti Registreeri Krediidiraportid.ee/Creditreports.ee on hetkel kättesaadav vaid registreeritud klientidele. Kasutajanimi Kasutajanimi või e-post Kasutajanimi/e-post Vale kasutajanimi ja/või salasõna registreeri siin 