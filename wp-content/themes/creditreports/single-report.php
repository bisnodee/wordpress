<?php get_header(); ?>
<section class="contentpage">
<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <?php $link = get_post_meta(get_the_ID(), 'download_pdf_link');?>
    <div class="widthlimit"><br/>
    <?php the_content(); ?>
        <a class="buy" href="<?php echo $link[0]?>">Download PDF</a></div>';
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>